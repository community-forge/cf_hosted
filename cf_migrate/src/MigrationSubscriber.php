<?php

namespace Drupal\cf_migrate;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\MigrateSkipRowException;

/**
 * Use bash's sshfs to make a viftual directory to migrate private files.
 * File '/var/aegir/migrate-temp/hostmaster_platform/sites/selclunisois.org/private/files/pictures/picture-22-1372225615.jpg' does not exist
 *       /var/aegir/migrate-temp/hostmaster_platform/sites/selclunisois.org/private/files/pictures/picture-22-1372225615.jpg
 */
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_row_save' => [['migratePreRowSave']],
      'migrate.post_row_save' => [['migratePostRowSave']],
      'migrate.post_import' => [['migratePostImport']],
      'migrate.pre_import' => [['migratePreImport']],
    ];
  }

  /**
   * @param Drupal\migrate\Event\MigrateImportEvent $event
   */
  public function migratePreImport(MigrateImportEvent $event) {
    // disable the mail when creating transactions
    if ($event->getMigration()->id() == 'd7_mc_transaction') {
      \Drupal::configFactory()->getEditable('mailsystem.settings')
        ->set('defaults.sender', 'test_mail_collector')
        ->save();
    }
    if ($event->getMigration()->id() == 'd7_mc_transaction') {
      foreach (\Drupal\node\Entity\Node::loadMultiple() as $node) {
        $node->delete();
      }
    }
  }

  /**
   * @param MigrateImportEvent $event
   */
  function migratePostImport(MigrateImportEvent $event) {
    // reenable the mail
    if ($event->getMigration()->id() == 'd7_mc_transaction') {
      \Drupal::configFactory()->getEditable('mailsystem.settings')
        ->set('defaults.sender', 'cforge_mail')
        ->save();
    }

    if ($event->getMigration()->id() == 'd7_cforge_settings') {
      \Drupal::configFactory()
        ->getEditable('cf_hosted.settings')
        ->set('fromd7', TRUE)
        ->save();
    }
  }

  /**
   * @param Drupal\migrate\Event\MigratePreRowSaveEvent $event
   */
  public function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $migration = $event->getMigration();
    $row = $event->getRow();
    // Make sure no organic groups fields are migrated
    $migs = ['d7_field_formatter_settings', 'd7_field_instance_widget_settings', 'd7_field', 'd7_field_instance'];
    if (in_array($migration->id(), $migs)) {
      $fname = $row->getSourceProperty('field_name');
      // also d7 field location maps to user__coordinates in module cforge_geo
      if (substr($fname, 0, 3) == 'og_') {
        throw new MigrateSkipRowException('Skipping og field '.$fname);
      }
    }
    // remove all references to mass contact.
    if ($migration->id() == 'd7_node_type') {
      if ($row->getSourceProperty('type') == 'mass_contact') {
        throw new MigrateSkipRowException('mass_contact module is not migrated.');
      }
    }
    elseif ($migration->id() == 'd7_field') {
      if (substr($row->getSourceProperty('field_name'), 0, 18) == 'field_mass_contact') {
        throw new MigrateSkipRowException('mass_contact fields ignored.');
      }
    }
    elseif ($migration->id() == 'd7_field_instance') {
      if (is_numeric(strpos($row->getSourceProperty('bundle'), 'mass_contact'))) {
        throw new MigrateSkipRowException('mass_contact fields ignored.');
      }
    }
    elseif ($migration->id() == 'd7_node') {
      if (substr($row->getSourceProperty('type') == 'mass_contact')) {
        throw new MigrateSkipRowException('mass_contact nodes ignored.');
      }
    }
    elseif ($migration->id() == 'd7_mcapi_credcom_settings') {
      $row->setDestinationProperty('trunkward.id', 'clearingcentral');
      $row->setDestinationProperty('trunkward.url', 'https://clearingcentral.net');
      $row->setDestinationProperty('trunkward.rate', 'https://clearingcentral.net');
    }
  }

  /**
   * Migrate the clearingcentral login
   * @depreecated
   */
  function migratePostRowSave(MigratePostRowSaveEvent $event) {
    if ($event->getMigration()->id() == 'd7_mcapi_clearing_central_settings') {
      $cc_nid = $event->getRow()->getDestinationProperty('login');
      // Assuming the fopen wrapper allows urls
      file_get_contents("https://www.clearingcentral.net/cfmove.php?nid=$cc_nid");
    }
  }

}
