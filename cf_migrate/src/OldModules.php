<?php

namespace Drupal\cf_migrate;

use Drupal\Core\Extension\ExtensionDiscovery;

/**
 * Drupal service invoked with cf_migrate.old_modules
 *  @todo injection
 */
class OldModules {

  const SERVER = '217.70.191.202';
  const USER = 'aegir';
  protected $d7Mods = [];
  private $dbPass;
  private $messenger;
  private $available;
  private $extensions;
  private $logger;
  private $moduleInstaller;

  CONST MAP = [
    'accountant_ui' => '',
    'addressfield' => 'address',
    'admin_menu' => 'admin_toolbar',
    'admin_menu_toolbar' => 'admin_toolbar_tools',
    'advanced_forum' => '',
    'aggregator' => 'aggregator',
    'admin_views' => '',
    'admin_views_system_display' => '',
    'backup_migrate' => '',
    'block' => 'block',
    'botcha' => '',
    'captcha' => '',// should already be enabled
    'cforge' => '',
    'cforge_basic_income' => '',
    'cforge_broadcast' => 'cforge_broadcast',
    'cforge_currencies' => '',
    'cforge_docs' => 'cforge_docs',
    'cforge_events' => 'cforge_events',
    'cforge_gallery' => 'cforge_gallery',
    'cforge_gdpr' => 'cf_gdpr',
    'cforge_geo' => 'cforge_network',
    'cforge_hosted' => 'cf_hosted',
    'cforge_launch' => '',
    'cforge_murmurations' => 'smallads_murmurations',
    'cforge_offline' => 'cforge_offline',
    'cforge_privacy' => '',
    'cforge_solsearch' => 'smallads_murmurations',
    'cforge_solsearch_init' => '',
    'cforge_yell' => '',
    'cfprod_smtp_headers' => 'cf_hosted',
    'cfpw' => '',
    'ckeditor' => '',
    'clone' => '',
    'color' => '',
    'comment' => 'comment',
    'community_tasks' => 'community_tasks',
    'contact' => 'contact',
    'content_access' => 'content_access',
    'contextual' => 'contextual',
    'css_injector' => 'asset_injector',
    'ctools' => '',
    'dashboard' => '',
    'date' => 'datetime',
    'date_all_day' => '',
    'date_api' => '',
    'date_popup' => '',
    'date_repeat' => '',
    'date_repeat_field' => '',
    'date_views' => '',
    'dblog' => 'dblog',
    'devel' => 'devel',
    'entity' => '',
    'entity_token' => 'token',
    'entitycache' => '',
    'entityreference' => '',
    'eu_cookie_compliance' => 'eu_cookie_compliance',
    'faq' => 'faq',
    'field' => '',
    'field_sql_storage' => '',
    'field_group' => 'field_group',
    'field_ui' => 'field_ui',
    'file' => 'file',
    'filter' => 'filter',
    'flexslider_fields' => '',
    'flexslider_views' => '',
    'forum' => 'forum',
    'gdpr' => 'gdpr',
    'geocoder' => '',
    'geofield' => 'geofield',
    'geofield_gmap' => 'geofield_map',
    'geophp' => '',
    'googleanalytics' => '',
    'help' => 'help',
    'hcaptcha' => 'hcaptcha',
    'hieararchical_select' => '',
    'honeypot' => '',
    'i18n' => 'content_translation',
    'i18n_block' => '',
    'i18n_contact' => '',
    'i18n_field' => '',
    'i18n_menu' => '',
    'i18n_node' => '',
    'i18n_path' => '',
    'i18n_redirect' => '',
    'i18n_select' => '',
    'i18n_string' => '',
    'i18n_sync' => '',
    'i18n_taxonomy' => '',
    'i18n_translation' => '',
    'i18n_user' => '',
    'i18n_variable' => '',
    'image' => 'image',
    'image_captcha' => 'image_captcha',
    'jquerymenu' => '',
    'leaflet' => 'leaflet',
    'libraries' => '',
    'list' => '',
    'locale' => 'locale',
    'mailsystem' => 'mailsystem',
    'masquerade' => 'masquerade',
    'masquerade_nominate' => 'masquerade_nominate',
    'mass_contact' => '',
    'mcapi' => 'mcapi',
    'mcapi_balance_history' => '',
    'mcapi_ajaxform' => '',
    'mcapi_cc' => 'mcapi_cc',
    'mcapi_forms' => 'mcapi_forms',
    'mcapi_import' => '',
    'mcapi_index_views' => '',
    'mcapi_limits' => 'mcapi_limits',
    'mcapi_signatures' => 'mcapi_signatures',
    'memcache' => 'memcache',
    'memcache_admin' => 'memcache',
    'menu' => '',
    'mimemail' => 'mimemail',
    'mimemail_action' => '',
    'node' => 'node',
    'nodeaccess' => 'content_access',
    'number' => '',
    'offers_wants' => 'smallads',
    'offers_wants_import' => '',
    'og_access' => '',
    'og_context' => '',
    'og_field_access' => '',
    'og_register' => '',
    'og_ui' => '',
    'options' => '',
    'ourtimebank' => '',
    'path' => 'path_alias',
    'poll' => 'poll', // on selclunisois, this wasn't enabled in d10...
    'potx' => '',
  #  'recaptcha' => 'recaptcha',
    'recaptcha' => '',
    'r4032login' => '',
  #  'role_expire' => 'role_expire',
    'role_expire' => '',
    'rules' => '',
    'search' => 'search',
    'search_api' => '',
    'search_api_facetapi' => '',
    'shs' => '',
    'smtp' => 'smtp',
    'social_share' => 'social_share',
    'syslog' => '',
    'shortcut'=> 'shortcut',
    'system' => 'system',
    'taxonomy' => 'taxonomy',
    'tb_demographics' => '',
    'text' => 'text',
    'token' => 'token',
    'tracker' => '',
    'translation' => '',
    'trigger' => '',
    'uid_login' => 'alt_login',
    'uif' => '',
    'update' => 'update',
    'user' => 'user',
    'user_chooser' => '',
    'usertabs' => '',
    'variable' => '',
    'variable_realm' => '',
    'variable_store' => '',
    'views' => 'views',
    'views_content' => '',
    'views_bulk_operations' => '',
    'views_data_export' => 'views_data_export',
    'webform' => '', // see below
    'webform_calculator' => '',
    'wysiwyg' => ''
  ];
  // not supported
  const NOT_SUPPORTED = [
    'acl' => '??',
    'statistics' => '',
    'calendar' => '',
    'php' => '',
    'l10n_update' => '',
    'moopapi' => 'moopapi',
    'stylizer' => '',
    'term_merge' => '',
    'rules_admin' => '',
    'synonyms' => 'synonyms',
    'taxonomy_manager' => ''
  ];

  const WONT_MIGRATE = [
    'cforge_control' => '',
    'blog' => "Blog module no longer exists in core; blog posts will be migrated but inaccessible",
    'chain_menu_access' => "Chain menu access is no longer required",
    'content_access_rules' => "Content access Rules is ignored because rules is not supported",
    'date_tools' => "Date Tools module may or may not be supported in d8 core.",
    'facetapi' => "faceted search cannot be migrated. Start again with https://www.drupal.org/project/facets",
    'flexslider' => "flexslider is not migrated, pictures must be replaced in to cforge's own slider",
    'forum_access' => "Forum access is not supported",
    'logintoboggan' => "Login will need to be reconfigured using the alt_login module at admin/config/people/alt_login",
    'l10n_client' => "l10n Client is not available for now",
    'number' => "Check migration of number fields...",
    //'rules' => "Rules cannot be migrated from d7 to d8. Please disable the module in the old site to continue, and hard-code rules if necessary."
    'poll' => "Migration of polls is not supported. Re-enable the poll module and reconfigure it.",
    'nodeaccess' => "Node access should have been replaced by content_access",
    'og' => "OG is not supported - use the migration scripts developed for SELidaire",
    'superfish' => "superfish is not supported by matslats",
    'synonyms_provider_field' => "synonyms_provider_field is not known to matslats",
    'synonyms_search' => "synonyms_search is not known to matslats",
    'views_ui' => "Sorry but customised views cannot be migrated. You will have to customise them again",
    'views_megarow' => "Views Megarow is not available for drupal8",
  ];

  CONST PROBLEM = [
    'role_expire' => '',
    'r4032login' => ''
  ];

  /**
   * Constructor.
   */
  function __construct($messenger, $logger, $root, $config_factory, $module_installer) {
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->extensions = $config_factory->get('core.extension');
    $this->moduleInstaller = $module_installer;
    $discovery = new ExtensionDiscovery($root);
    $this->available = array_keys($discovery->scan('module'));
    $this->d7Mods = cf_migrate_setup_db()->query("SELECT name FROM system WHERE status = 1 AND type = 'module' order by name asc")->fetchCol();
  }

  /**
   *
   * @param type $old_db_name
   */
  function enableModules() {

    $user_notes[] = 'The following notes should be forward to the site manager.';
    // Show a warning for any d7 modules in the won't migrate list.
    foreach (array_intersect($this->d7Mods, array_keys(SELF::WONT_MIGRATE)) as $mod) {
      if (isset(SELF::WONT_MIGRATE[$mod])) {
        $user_notes[] = t("Module @mod won't be migrated because: (@reason)", ['@mod' => $mod, '@reason' => SELF::WONT_MIGRATE[$mod]]);
        $key = array_search($mod, $this->d7Mods);
        unset($this->d7Mods[$key]);
      }
    }

    // Special warning for i18n modules
    if (in_array('i18n', $this->d7Mods)) {
      $user_notes[] = "i18n migration is NOT TESTED";
    }
    if (in_array('css_injector', $this->d7Mods)) {
      $user_notes[] = 'css_injector will remain installed but all custom css (for sky_seldlac) will be lost. You can salvage the css from the old site before finishing the migration. The D7 css will not work but could serve as a reminder if it needs to be re-applied.';
    }

    // Stop execution if any d7 modules are on the problem list.
    if ($stop = array_intersect($this->d7Mods, SELF::PROBLEM)) {
      $user_notes[] = t(
        "The following modules will be ignored because Matthew didn't anticipate them. If you cannot live without them, tell him: @modules",
        ['@modules' => implode(', ', $stop)]
      );
    }
    $map = SELF::MAP;
    if (in_array('webform', $this->available)) {
      $map['webform'] = 'webform';
    }


    // Start building the list of modules to install in d8.
    $d8mods = $skip = [];
    $d8mods[] = 'cforge_address';
    // Build the list of d8 modules to install, stopping in case of unknown modules.
    foreach ($this->d7Mods as $d7_mod) {
      if (!empty($map[$d7_mod])) {
        $d8mods[] = $map[$d7_mod];
      }
      elseif(isset(SELF::NOT_SUPPORTED[$d7_mod])) {
        $skip[] = $d7_mod;
      }
      elseif (!isset($map[$d7_mod])) {
        $user_notes[] = "Unexpected D7 module $d7_mod will be ignored";
      }
    }

    // D8 webform module was broken into several modules.
    // There's some question about whether webform can be assumed to be present on cforge platform
    if(in_array('webform', $d8mods)) {
      $d8mods[] = 'webform_migrate';
      $d8mods[] = 'webform_node';
      $d8mods[] = 'webform_ui';
    }
    elseif (in_array('webform', $d8mods)) {
      $skip[] = 'webform';
    }

    if ($skip) {
      $user_notes[] = "Skipping modules ".implode(', ', $skip);
    }
    $d8mods = array_diff($d8mods, array_keys($this->extensions->get('module')));

    // check if any modules are not present
    if ($missing = array_diff($d8mods, $this->available)) {
      $user_notes[] = "The following modules are missing from the platform. Consider adding them via composer before proceeding: ".implode(', ', $missing);
    }
    $this->messenger->addWarning(implode('<br />', $user_notes));
    if ($d8mods) {
      $this->moduleInstaller->install($d8mods);
      $this->logger->notice("Installed ".implode(', ', $d8mods));
      $this->messenger->addStatus("Installed ".implode(', ', $d8mods));
    }
    else {
      $this->messenger->addStatus('No new modules were installed from the old site');
    }
  }
}
