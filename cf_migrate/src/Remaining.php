<?php

namespace Drupal\cf_migrate;

use Drupal\Core\Form\FormBase;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
/**
 * Configuration options for each flavour.
 *
 */
class Remaining extends FormBase {

  private $todo = [];
  private $done = [];
  private $connection; // not used???

  function __construct() {
    // ensure the database connection is available.
    $this->connection = cf_migrate_setup_db();
    $this->makeMigrationLists();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->todo) {
      $form['migrations'] = [
        '#type' => 'tableselect',
        '#header' => [
          'id' => t('Migration ID'),
          'done' => t('Remaining'),
          'dependencies' => t('Outstanding dependencies'),
        ],
        '#sticky' => true,
        '#options' => []
      ];
      foreach ($this->todo as $id => $details) {
        $outstanding_dependencies = array_intersect($details['dependencies'], array_keys($this->todo));
        $form['migrations']['#options'][$id] = [
          'id' => [
            'data' => $id
          ],
          'done' => [
            'data' => strval($details['unprocessed'] .'/'. $details['total']),
            'style' => 'text-align:center;'
          ],
          'dependencies' => [
            'data' => Markup::create(implode('<br />', $outstanding_dependencies))
          ],
          '#disabled' => (bool)$outstanding_dependencies
        ];
        if ($details['status'] <> MigrationInterface::STATUS_IDLE) {
          $form['migrations']['#options'][$id]['id']['data'] .= ' '.strtoUpper($details['status_label']);
          $form['migrations']['#options'][$id]['id']['style'] = 'color:red';
        }
      }

      $form['run_many'] = [
        '#type' => 'submit',
        '#value' => t('Run selected migrations'),
        '#name' => 'run_many',
        '#submit' => ['::submitRunMany'],
        '#weight' => 10,
      ];
    }

    $form['finish'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => 'Finalise (@todo - Find how to run migrate-upgrade programmatically instead of visiting /upgrade)',
      '#name' => 'uninstall',
      '#disabled' => TRUE, // this should only be enabled once all listed migrations have started and are idle
      '#description' => 'Migrate latest items, switch DNS in cloudflare, Disable migration modules, notify committee, register with Aegir.',
      'submit' => [
        '#type' => 'submit',
        '#value' => t('Finalise migration'),
        '#submit' => ['::submitFinalise'],
      ],
      '#weight' => 11
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitRunMany(array &$form, FormStateInterface $form_state) {
    ini_set('max_execution_time', 200);
    $migration_ids = array_filter($form_state->getValue('migrations'));
    $migrations = \Drupal::service('plugin.manager.migration')->createInstances($migration_ids);
    foreach ($migrations as $migration) {
      $this->reset($migration);
      $executable = new MigrateExecutable($migration, new CfMigrateMessage());
      try {
        $executable->import();
        // Can't tell from here
        \Drupal::messenger()->addStatus(t('Migration @migration_id has run.', ['@migration_id' => $migration->id()]));
      }
      catch (Exception $e) {
        \Drupal::messenger()->addError(t('Migration @migration_id failed to run: @error', [
          '@migration_id' => $migration->id(),
          '@error' => $e->getMessage(),
        ]));
      }
    }
  }

  /**
   * Form submit handler.
   * - Clean up blocks.
   * - Migrate latest items
   * - Switch DNS in cloudflare
   * - Disable migration modules
   * - Notify committee
   *
   * @todo
   */
  public function submitFinalise(array &$form, FormStateInterface $form_state) {
    // remove blocks with broken plugins
    foreach (\Drupal\Core\Block\Plugin\Block::loadMultiple() as $block) {
      try {
        $plugin = $block->getPlugin();
      }
      catch (\Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
        $block->delete();
      }
    }
  }

  /**
   * List all the unfinished migrations
   * @param int $run
   *  Number of migrations to run before relisting.
   */
  private function makeMigrationLists() {
    // Loop through each migration and get its status.
    foreach (\Drupal::service('plugin.manager.migration')->createInstances([]) as $migration_id => $migration) {
      $report = $this->report($migration);
      if ($migration->getStatus() <> MigrationInterface::STATUS_IDLE or $report['unprocessed'] > 0) {
        $this->todo[$migration_id] = $report;
      }
    }

    // order migrations with the least dependencies first.
    uasort(
      $this->todo,
      fn($a, $b) => count($a['dependencies']) <=> count($b['dependencies'])
    );

  }


  /**
   * Generate an array showing the current status of a migration
   */
  private function report(MigrationInterface $migration) : array {
    $map = $migration->getIdMap();
    $source = $migration->getSourcePlugin();
    return [
      'status' => $migration->getStatus(),
      'status_label' => $migration->getStatusLabel(),
      'total' => $source->count(),
      'imported' => $map->importedCount(),
      'unprocessed' => $source->count() - $map->processedCount(),
      'messages' => $map->messageCount(),
      'tags' => $migration->getMigrationTags(),
      'dependencies' => $migration->getRequirements()
    ];
  }

  /**
   * Reset, rollback and run a migration.
   * @param Migration $migration
   */
  private function reset($migration) {
    $status = $migration->getStatus();
    if ($status == MigrationInterface::STATUS_IMPORTING or $status == MigrationInterface::STATUS_ROLLING_BACK) {
      \Drupal::messenger()->addStatus('Resetting status of '.$migration->id().' to idle');
      $migration->setStatus(MigrationInterface::STATUS_IDLE);
    }
    $executable = new MigrateExecutable($migration, new CfMigrateMessage());
    if ($status == MigrationInterface::STATUS_IMPORTING) {
      \Drupal::messenger()->addStatus('Rolling back '.$migration->id());
      $executable->rollback();
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //not used
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cf_migrate_form';
  }

}


/**
 * Print a migration message to screen as well as to the log.
 */
class CfMigrateMessage extends MigrateMessage {
  public function display($message, $type = 'status') {
    \Drupal::messenger()->addStatus((string)$message, $type);
    parent::display($message, $type);// log as notice
  }
}
