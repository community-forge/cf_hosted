<?php

namespace Drupal\cf_migrate;

/**
 * Class to inspect the completed migrations.
 */
class Messages extends \Drupal\Core\Controller\ControllerBase {


  /**
   * Show all the migration messages on one page.
   */
  function page(string $migration) {
    $table_name = 'migrate_message_'.str_replace(':', '__', $migration);
    // The link shouldn't be available in the first place.
    if (!\Drupal::database()->schema()->tableExists($table_name)) {
      return ['#markup' => 'No messages'];
    }
    $messages = \Drupal::database()
      ->select($table_name, 'm')
      ->fields('m', ['message'])
      ->execute()
      ->fetchCol();
    // This page should not be linked if there are no messages.
    return [
      '#markup'=> implode('<br />', $messages)
    ];
  }

}

