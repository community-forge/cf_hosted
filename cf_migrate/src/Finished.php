<?php

namespace Drupal\cf_migrate;
use Drupal\Core\Form\FormBase;
use \Drupal\Core\Link;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Class to inspect the completed migrations.
 */
class Finished extends FormBase {

  /**
   *  {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['done'] = [
      '#type' => 'tableselect',
      '#header' => [
        'id' => t('Migration ID'),
        'total' => t('Total'),
        'imported' => t('Imported'),
        'messages' => t('Messages')
      ],
      '#sticky' => true,
      '#options' => [],
      '#weight' => 1
    ];

    $instances = \Drupal::service('plugin.manager.migration')->createInstances([]);
    foreach ($instances as $migration_id => $migration) {
      $map = $migration->getIdMap();
      $source = $migration->getSourcePlugin();

      if ($migration->getStatus() == MigrationInterface::STATUS_IDLE and $source->count() - $map->processedCount() < 1) {
        $migrations[$migration_id] = [
          'migration' => $migration,
          'map' => $map,
          'source' => $source
        ];
      }
    }
    foreach ($migrations as $id => $data) {
      if ($count = $this->countMessages($id)) {
        $link = Link::createFromRoute(
          t('See @count messages', ['@count' => $count]),
          'cf_migrate.messages',
          ['migration' => $id],
          ['attributes' => ['target' => 'messages']]
        )->toString();
      }
      $form['done']['#options'][$id] = [
        'id' => $id,
        'total' => $data['source']->count(),
        'imported' => $data['map']->importedCount(),
        'messages' =>  $count ? $link : ''
      ];
    }

    $form['rollback'] = [
      '#type' => 'submit',
      '#value' => 'Rollback selected',
      '#weight' => 10
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach (array_filter($form_state->getValue('done')) as $migration_id) {
      $migration = \Drupal::service('plugin.manager.migration')->createInstance($migration_id);
      $executable = new MigrateExecutable($migration, new MigrateMessage());
      $executable->rollback();
      \Drupal::database()->truncate("migrate_message_$migration_id");
    }
    $form_state->setRedirect('cf_migrate.remaining');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cf_migrate_finished_form';
  }


  /**
   * Because $migration->getIdMap()->messageCount() doesn't seem to work.
   */
  private function countMessages($mig_id) : int {
    $table_name = 'migrate_message_'.str_replace(':', '__', $mig_id);
    // Sometimes tables are missing.
    if (!\Drupal::database()->schema()->tableExists($table_name)) {
      return 0;
    }
    return \Drupal::database()
      ->select($table_name, 'm')
      ->fields('m', ['message'])
      ->countQuery()
      ->execute()
      ->fetchField();
  }

}

