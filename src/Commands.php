<?php

namespace Drupal\cf_hosted;

use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Drush Commands
 */
class Commands extends DrushCommands {

  /**
   * Restores the site after migration and notify committee
   *
   * @command cf_hosted:grant-user1
   * @todo
   */
  public function grantUser1() {
    \Drupal::service('state')->set('system.maintenance_mode', TRUE);
    $committee = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->loadByProperties(['roles' => 'committee']);
    foreach ($committee as $account) {
      $mails[] = $account->getEmail();
    }

    if (\Drupal::service('language.default')->get()->id() == 'fr') {
      //@todo put english and french in private.yml
    }
    else {
      $subject = 'Your site is upgraded';
      $body[] = "We're proud to announce, after 6 years of preparation that your site is running on Drupal 8";
      $body[] = "The migration process was complex and may not be perfect though, so you need to go through our checklist...";
    }
    $addresses = implode(', ', $mails);
    if (mail($addresses, $subject, $body)) {
      drush_print('Notified '.$addresses);
    }
    \Drupal::service('state')->set('system.maintenance_mode', FALSE);
  }

}
