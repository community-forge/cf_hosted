<?php

namespace Drupal\cf_hosted;

use Drupal\Core\Serialization\Yaml;

/**
 * Service 'cf_hosted.private_strings'
 */
class PrivateData {

  private $moduleHandler;
  private $logger;
  private $messenger;
  private $data;

  function __construct($module_handler, $logger, $messenger) {
    $this->moduleHandler = $module_handler;
    $this->logger = $logger;
    $this->messenger = $messenger;

        // The file is outside of the web root and common to all platforms.
    if (!file_exists(CF_HOSTED_PRIVATE_DATA_STORAGE)) {
      throw new \Exception('Password file not present at'. realpath(SELF::STORAGE));
    }
    $this->data = Yaml::decode(file_get_contents(CF_HOSTED_PRIVATE_DATA_STORAGE));
  }

  /**
   * Get the password from a file stored outside the site root.
   * @param string $key
   * @return string
   */
  function get($key) {
    if (isset($this->data[$key])) {
      return $this->data[$key];
    }
    $this->messenger->addError('Unable to fetch protected data: '.$key);
    return '';
  }

}

