<?php

namespace Drupal\cf_hosted;

use Drupal\Core\Entity\EntityAccessCheck;
use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Class to protect access to the privacy polcy node, which was imported by
 * cforge.profile
 */

class NeverChangeEntityAccessCheck extends EntityAccessCheck {

  /**
   * Prevent access to specific nodes and menu items
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $parameters = $route_match->getParameters();
    $key = $parameters->keys()[0];
    if ($parameters->get($key)->uuid() == $route->getRequirement('_cf_no_change')) {
      return AccessResult::forbidden();
    }
    return parent::access($route, $route_match, $account);
  }
}

