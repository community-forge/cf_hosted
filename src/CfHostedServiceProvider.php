<?php

namespace Drupal\cf_hosted;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the language manager service.
 */
class CfHostedServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('exception.logger')
      ->setClass('\Drupal\cf_hosted\EventSubscriber\ExceptionLoggingSubscriber');
  }

}
