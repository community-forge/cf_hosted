<?php

namespace Drupal\cf_hosted\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/*
 * ZIP errors
\ZipArchive::ER_EXISTS File already exists.
\ZipArchive::ER_INCONS Zip archive inconsistent.
\ZipArchive::ER_INVAL Invalid argument.
\ZipArchive::ER_MEMORY Malloc failure.
\ZipArchive::ER_NOENT No such file. 9
\ZipArchive::ER_NOZIP Not a zip archive.
\ZipArchive::ER_OPEN Can't open file.
\ZipArchive::ER_READ Read error.
\ZipArchive::ER_SEEK Seek error.
 *
 */

/**
 * Download a zip file with database dump and all files
 *
 * @todo can the results of "drush @$SITENAME archive-dump $SITENAME --no-core" be sent to the browser?
 */
class Backup {

  const FILENAME = "backup.zip";

  /**
   *
   * @var string
   */
  private $rootDir;

  /**
   *
   * @var string
   */
  private $sitesDir;

  /**
   * Had some permissions trouble using the default temp dir.
   * @var string
   */
  private $tempFilePath;

  function __construct() {
    $this->sitesDir = \Drupal::service('site.path');
    $this->rootDir = dirname(\Drupal::root());
    $parts = explode('/', $this->sitesDir);
    $this->tempFilePath = $this->rootDir .'/tmp/'. array_pop($parts) .'/'.SELF::FILENAME;
  }

  /**
   * Zip up the sites folder along with the composer.json and database dump.
   */
  function download() {
    $z = new \ZipArchive();
    $z->open($this->tempFilePath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
    $z->addFile($this->rootDir.'/composer.json', 'composer.json');
    $z->addFile(__DIR__.'/README.txt', 'README.txt');

    // Dump the database and zip it.
    $db_creds = (object)\Drupal::database()->getConnectionOptions();
    exec("mysqldump $db_creds->database -u$db_creds->username -p$db_creds->password --host $db_creds->host > $this->sitesDir/backup.sql");
    exec("zip $this->sitesDir/backup.sql.zip $this->sitesDir/backup.sql");
    unlink("$this->sitesDir/backup.sql");

    /** @var SplFileInfo[] $files */
    $files = new \RecursiveIteratorIterator(
      new \RecursiveDirectoryIterator($this->sitesDir),
      \RecursiveIteratorIterator::LEAVES_ONLY
    );
    foreach ($files as $name => $file) {
      if ($file->isDir() or preg_match('/\/php|js|css|styles\//', $file->getPathname())) {
        continue;
      }
      $filePath = $file->getRealPath();
      // Add to archive with relative path.
      $z->addFile($filePath, substr($filePath, strlen($this->rootDir) + 1));
    }

    $success = $z->close(); // Failure to create temporary file: Permission denied???
    unlink("$this->sitesDir/backup.sql.zip");
    if ($success) {
      $response = new BinaryFileResponse($this->tempFilePath, Response::HTTP_OK);
      // @todo how to we send a temporary file which doesn't need deleting?
      //unlink($z->filename);
    }
    else {
      \Drupal::messenger()->addError("Failed to close zip archive. Check error log.");
      $response = new RedirectResponse('/');
    }
    return $response;
  }

}