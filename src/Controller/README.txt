How to install your own site from this backup
You will need some familiarity with Drupal and composer.
Unzip this archive so that your web root is the web/directory contained.
Make sure the web/sites/MYSITE directory is writable by the web server.
From the directory above, with the composer file in it, run on the command line:
composer install --no-dev
Need to test more...
Clear cache?
If your domain name has changed you need to edit web/sites/MYSITE/settings.php.
May want to rename web/sites/MYSITE to web/sites/default.
