<?php

namespace Drupal\cf_hosted;

use Drupal\user\Entity\User;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\Config;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Handle all aspects of communication with the postfix mail server.
 */
class MailAliases {

  const SUBDOMAIN = 1;
  const DOMAIN = 0;

  private bool $format;

  /**
   * @var Config
   */
  private $siteConfig;

  /**
   * @var Database
   */
  public $connection;

  /**
   * @var array
   */
  private $dbConfig = [
    'driver' => 'mysql', // The database driver (e.g., 'mysql', 'pgsql', 'sqlite').
    'database' => 'mailserver', // The name of the database.
    'host' => 'mail.communityforge.net',
    'port' => '3306', // The database port.
  ];

  /**
   * The name of the website from the Request
   * @var string
   */
  private $hostName;

  /**
   * The domain under which the aliases are stored, either the domain, or communityforge.net for subdomains.
   * @var string
   */
  private $aliasDomain;

  /**
   * The first part of the email alias, before the uid.
   * @var string
   */
  private $aliasPrefix;

  /**
   * Time in the format preferred by postfixadmin
   * @var string
   */
  private $now;

  /**
   * Constructor
   *
   * @param ConfigFactory $config_factory
   * @param Request $request_stack
   */
  public function __construct(ConfigFactory $config_factory, RequestStack $request_stack, $cf_private_) {
    $this->siteConfig = $config_factory->get('system.site');
    $this->dbConfig['username'] = $cf_private_->get('mail_server_login');
    $this->dbConfig['password'] = $cf_private_->get('mail_server_pw');
    $this->now = date('Y-m-d H:i:s');
    $this->hostName = str_replace('www.', '', $request_stack->getCurrentRequest()->getHttpHost());
    // This will totally mess up is the site is accessed from different points. i.e. if apache isn't redirecting.
    if (str_ends_with($this->hostName, '.communityforge.net')) {
      $this->format = self::SUBDOMAIN;
      $this->aliasDomain = 'communityforge.net';
      $this->aliasPrefix = str_replace(['.communityforge.net', '-'], ['cf', ''], $this->hostName);
    }
    else {
      $this->format = self::DOMAIN;
      #$this->aliasDomain = $this->hostName;
      #$this->aliasPrefix = 'member';
      $this->aliasPrefix = str_replace(['.', '-'], ['', ''], $this->hostName);
      $this->aliasDomain = 'communityforge.net';
    }
    if (isset($_SERVER['SERVER_ADDR'])) { // think this works with cron. Intended not to work with drush import
      // Prevent making the remote database connection if the site is not on a public server.
      if (filter_var($_SERVER['SERVER_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {
        Database::addConnectionInfo('mail', 'mail', $this->dbConfig, 'mail_server');
        $this->connection = Database::getConnection('mail', 'mail');
      }
    }
  }

  /**
   * Register a new site by making a new domain and catch-all alias.
   */
  public function registerSite() {
    if ($this->connection) {
      $this->connection->merge('alias')
        ->key(['address' => $this->aliasPrefix . '@communityforge.net'])
        ->fields([
          'goto' => $this->siteConfig->get('mail'),
          'domain' => 'communityforge.net',
          'created' => $this->now,
          'modified' => $this->now,
          'active' => 1
      ])->execute();
      // In most cases the site will already exist from D7
      $this->connection->merge('domain')
        ->key(['domain' => $this->hostName])
        ->fields([
          'description' => 'auto (d10)',
          'aliases' => $this->format == self::DOMAIN ? -1 : 1, //-1 means unlimited.
          'mailboxes' => 0,
          'transport' => 'virtual',
          'created' => $this->now,
          'modified' => $this->now,
          'active' => 1
      ])->execute();
      // Add the catchall alias redirecting to the system.site.mail
      $this->connection->merge('alias')
        ->key(['address' => '@'.$this->hostName])
        ->fields([
          'address' => '@'.$this->hostName,
          'goto' => $this->siteConfig->get('mail'),
          'domain' => $this->hostName,
          'created' => $this->now,
          'modified' => $this->now,
          'active' => 1
        ])
        ->execute();
    }
  }

  /**
   * Update all email aliases on the mail server.
   *
   * @param array $existing_users
   *   Mails of active users keyed by uid.
   */
  public function update(array $existing_users) {
    if ($this->hostName == 'communityforge.net')return;
    $existing_users = array_filter($existing_users); // this should never happen.
    if ($this->connection) {
      // Touch the domain table so we can see the domain is alive.
      $this->connection->update('domain')
        ->fields(['modified' => $this->now, 'description' => 'auto (d10)'])
        ->condition('domain', $this->hostName)
        ->execute();

      $aliases = $this->connection->select('alias', 'a')
        ->fields('a', ['address', 'goto'])
        ->condition('address', $this->aliasPrefix .'[0-9]+@communityforge.net', 'RLIKE')
        ->condition('domain', $this->aliasDomain)
        ->execute()->fetchAllKeyed(0); // alias => realmail

      if ($removed_mails = array_diff($aliases, $existing_users)) { // Keys of $aliases are preserved i.e. the alias
        $this->connection->delete('alias')
          ->condition('address', array_keys($removed_mails), 'IN')
          ->condition('domain', $this->aliasDomain)
          ->condition('address', $this->aliasPrefix . '@'.$this->aliasDomain, '<>')// the site_mail
          ->execute();
        \Drupal::logger('cf_hosted')->notice(
          'Removed @count aliases from the mail server',
          ['@count' => count($removed_mails)]
        );
      }
      // Get the mails to delete and mails to insert.
      if ($new_mails = array_diff($existing_users, $aliases)) {// Keys of $existing_users are preserved i.e. the uid
        foreach ($new_mails as $uid => $real_email) {
          $this->insertAlias($this->constructAlias($uid), $real_email);
        }
        \Drupal::logger('cf_hosted')->notice(
          'written @count aliases to the mail server',
          ['@count' => count($new_mails)]
        );
      }
    }
  }

  /**
   * Get a formatted string for the user's email, with name.
   * @param int $uid
   * @param string $name
   * @return string
   */
  public function getUserAlias(int $uid, string $name = NULL) : string {
    if (!$name) {
      $name = User::load($uid)->getDisplayName();
    }
    return $this->stripAccented($name) .'<'. $this->constructAlias($uid) .'>';
  }

  /**
   * Check if an email is on a domain hosted by cforge.
   * @param string $email
   * @return bool
   */
  public function isCforgeMail(string $email) : bool {
    return strpos($email, '@communityforge.net');
  }

  /**
   * Get the alias (or even the actual) site_mail.
   * @return string
   */
  public function getSiteAlias() : string {
    if ($this->format == self::DOMAIN) {
      // If the site mail is within the current domain, use that directly
      if (strpos($this->siteConfig->get('mail'), $this->aliasDomain)) {
        $site_alias = $this->siteConfig->get('mail');
      }
      else {
        // N.B. this is handled by the wildcard.
        $site_alias = $this->aliasPrefix .'@'. $this->aliasDomain;
      }
    }
    else {
      // Use the prefix alone (without the uid)
      $site_alias = $this->aliasPrefix .'@'. $this->aliasDomain;
    }
    return $site_alias;
  }

  /**
   * @param string $external_mail
   */
  public function setCatchAll(string $external_mail) {
    if ($this->connection and !str_ends_with('communityforge.net', $this->hostName)) {
      $this->connection->merge('alias')
        ->key(['address' => '@'.$this->hostName])
        ->fields([
            'goto' => $external_mail,
            'domain' => $this->hostName,
            'active' => 1,
            'created' => $this->now,
            'modified' => $this->now
          ])
        ->execute();
    }
  }

  /**
   * @param string $address
   * @param string $goto
   */
  public function insertAlias(string $address, string $goto) {
    if ($this->connection) {
      $this->connection->merge('alias')
        ->key(['address' => $address])
        ->fields([
            'goto' => $goto,
            'domain' => $this->aliasDomain,
            'active' => 1,
            'created' => $this->now,
            'modified' => $this->now
          ])
        ->execute();
    }
  }

  /**
   * @param int $uid
   * @return string
   */
  public function constructAlias(int $uid) : string {
    return $this->aliasPrefix . $uid .'@'. $this->aliasDomain;
  }

  /**
   * Utility to remove the accented chars from a string.
   * @param string $string
   * @return string
   */
  private function stripAccented(string $string) : string {
    $translation_table = array_combine(
      ['À','Á','Â','Ã','Ä','Å','È','É','Ê','Ë','Ì','Í','Î','Ï','Ò','Ó','Ô','Õ','Ö','Ù','Ú','Û','Ü','à','á','â','ã','ä','å','è','é','ê','ë','ì','í','î','ï','ò','ó','ô','õ','ö','ù','ú','û','ü'],
      ['A','A','A','A','A','A','E','E','E','E','I','I','I','I','O','O','O','O','O','U','U','U','U','a','a','a','a','a','a','e','e','e','e','i','i','i','i','o','o','o','o','o','u','u','u','u']
    );
    return strtr($string, $translation_table);
  }

}

