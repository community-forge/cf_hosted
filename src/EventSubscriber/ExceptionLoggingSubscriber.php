<?php
namespace Drupal\cf_hosted\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionLoggingSubscriber extends \Drupal\Core\EventSubscriber\ExceptionLoggingSubscriber {

  /**
   * Only log 404 errors for authenticated users.
   * @param ExceptionEvent $event
   * not working.
   */
  public function on404(ExceptionEvent $event) {
    if (\Drupal::currentUser()->isAuthenticated()) {
      parent::on404($event);
    }
  }

}
