<?php

namespace Drupal\cf_hosted\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber to create a router item for each transaction form display.
 */
class RouteSubscriber extends RouteSubscriberBase {

  private $profile;

  /*
   * @param string $install_profile
   *   The install profile used by the site.
   */
  public function __construct($install_profile) {
     $this->profile = $install_profile;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($this->profile == 'cforge') {
      $collection->get('entity.node.delete_form')
        ->setRequirement('_cf_no_change', 'd7a857c3-8744-43b2-ab45-2093de04809a');
      $collection->get('entity.node.edit_form')
        ->setRequirement('_cf_no_change', 'd7a857c3-8744-43b2-ab45-2093de04809a');
      $collection->get('entity.menu_link_content.edit_form')
        ->setRequirement('_cf_no_change', '3d5f1bc2-2c2b-4df3-b366-b921a5549397');
      $collection->get('entity.menu_link_content.delete_form')
        ->setRequirement('_cf_no_change', '3d5f1bc2-2c2b-4df3-b366-b921a5549397');
      $collection->get('entity.menu_link_content.canonical')
        ->setRequirement('_cf_no_change', '3d5f1bc2-2c2b-4df3-b366-b921a5549397');
    }
  }

}
