<?php

namespace Drupal\cf_hosted\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\migrate\MigrateSkipRowException;

class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.post_import' => [['migratePostImport']],
      'migrate.pre_row_save' => [['migratePreRowSave']]
    ];
  }

  /**
   * @param Drupal\migrate\Event\MigratePreRowSaveEvent $event
   */
  public function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $mid = $event->getMigration()->id();
    if ($mid == 'd7_node:page' or $mid == 'd7_node:page') {
      $row = $event->getRow();
      if ($row->getSourceProperty('title') == t('Privacy policy')) {
        throw new MigrateSkipRowException('Not migrating privacy policy page.');
      }
      // Let the old node 1 become a new node. (see migratePostImport in case it was the front page.)
      elseif ($row->getSourceProperty('nid') == PRIVACY_POLICY_NID) {
        $row->removeDestinationProperty('nid');
        $row->removeDestinationProperty('vid');
        $event->logMessage("Old node 1 has been saved with a new nid so as not to overwrite the privacy policy" , 'status');
      }
    }
  }

  /**
   * @param MigrateImportEvent $event
   */
  function migratePostImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'd7_node:page') {
      $system_site_settings = \Drupal::configFactory()->getEditable('system.site');
      $old_front = $system_site_settings->get('page.front');
      if (preg_match('/\/node\/([0-9]+)/', $old_front, $matches)) {
        $nid = $matches[1];
        $new_nid = \Drupal::database()->select('migrate_map_d7_node__page', 'map')->fields('map', ['destid1'])->condition('sourceid1', $nid)->execute()->fetchField();
        if ($new_nid) {
          $system_site_settings->set('page.front', '/node/'.$new_nid)->save();
          $event->logMessage("changed front page from $old_front to /node/$new_nid" , 'status');
        }
        else {
          $event->logMessage("Unable to find new nid for old front page ".$matches[1] , 'status');
        }
      }
    }
  }
}
