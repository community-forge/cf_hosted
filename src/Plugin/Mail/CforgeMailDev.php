<?php

namespace Drupal\cf_hosted\Plugin\Mail;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Mail\MailFormatHelper;
use Drupal\Core\Render\Markup;
use Drupal\mysql\Driver\Database\mysql\Connection;
use Drupal\cf_hosted\MailAliases;
use Drupal\Core\Logger\LoggerChannel;


/**
 * Format the mail and send it to the screen instead of sendmail
 *
 * @Mail(
 *   id = "cforge_mail_dev",
 *   label = @Translation("Cforge MimeMailer DEV-mode"),
 *   description = @Translation("Prints messages to screen")
 * )
 */
class CforgeMailDev extends CforgeMail {

  /**
   * to print the mail to screen.
   */
  protected MessengerInterface $messenger;

 /**
  * MimeMail plugin constructor.
  *
  * @param type $config_factory
  * @param type $module_handler
  * @param type $email_validator
  * @param type $renderer
  * @param Connection $database
  * @param MailAliases $mail_aliases
  * @param LoggerChannel $logger
  * @param MessengerInterface $messenger
  */
  function __construct($config_factory, $module_handler, $email_validator, $renderer, Connection $database, MailAliases $mail_aliases, LoggerChannel $logger, MessengerInterface $messenger) {
    parent::__construct($config_factory, $module_handler, $email_validator, $renderer, $database, $mail_aliases, $logger);
    $this->messenger = $messenger;
    if ($config_factory->get('system.logging')->get('error_level') == 'hide') {
      $config_factory->getEditable('system.logging')->set('error_level', 'all')->save();
    }
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('email.validator'),
      $container->get('renderer'),
      $container->get('database'),
      $container->get('cf_hosted.mail_aliases'),
      $container->get('logger.channel.cf_hosted'),
      $container->get('messenger')
    );
  }

  /**
   * Format the mail for screen
   */
  public function format(array $message) {
    $eol = PHP_EOL;
    $boundary = uniqid('np');

    // Join the body array into one string.
    $html = implode("\n\n", $message['body']);

    $message['headers']['Content-Type'] = 'multipart/alternative;boundary="' . $boundary . '"';
    //$message['headers']['MIME-Version'] = '1.0';

    $raw_message = 'This is a MIME encoded message.';
    $raw_message .= $eol . $eol . "--" . $boundary . $eol;
    $raw_message .= "Content-Type: text/plain;charset=utf-8" . $eol . $eol;
    $raw_message .= MailFormatHelper::htmlToText($html);
    $raw_message .= $eol . $eol . "--" . $boundary . $eol;
    $raw_message .= "Content-Type: text/html;charset=utf-8" . $eol . $eol;
    $raw_message .= $html;
    $raw_message .= $eol . $eol . "--" . $boundary . "--";
    $message['body'] = $raw_message . '<hr>';
    return $message;
  }

  /**
   * {@inheritdoc}
   *
   * Don't send mail to example users.
   */
  public function mail(array $message) {
    $output[] = "to: ".$message['to'];
    $output[] = "from: ".$message['from'];
    foreach ($message['headers'] as $key => $value) {
      $output[] = "$key: $value";
    }
    $output[] = '';
    $output[] = $message['body'];
    // This works for plaintext mails.
    $markup = Markup::create('<pre>'.implode('<br>', $output).'</pre><hr />');
    $this->messenger->addStatus($markup);
    return TRUE;
  }

}
