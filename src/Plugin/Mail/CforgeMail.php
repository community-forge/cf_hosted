<?php

namespace Drupal\cf_hosted\Plugin\Mail;

use Drupal\mimemail\Plugin\Mail\MimeMail;
use Drupal\cf_hosted\MailAliases;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\mysql\Driver\Database\mysql\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines cforge's mail function, mainly by altering headers
 *
 * @Mail(
 *   id = "cforge_mail",
 *   label = @Translation("Cforge MimeMailer"),
 *   description = @Translation("Sends MIME-encoded emails with embedded images and attachments.")
 * )
 */
class CforgeMail extends MimeMail {

  protected Connection $database;
  protected MailAliases $mailAliases;
  protected LoggerChannel $logger;

  /**
   * @param Connection $database
   * @param MailAliases $mailAliases
   * @param LoggerChannel $logger
   */
  function __construct($config_factory, $module_handler, $email_validator, $renderer, Connection $database, MailAliases $mail_aliases, LoggerChannel $logger) {
    parent::__construct($config_factory, $module_handler, $email_validator, $renderer);
    $this->database = $database;
    $this->mailAliases = $mail_aliases;
    $this->logger = $logger;
  }


  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('email.validator'),
      $container->get('renderer'),
      $container->get('database'),
      $container->get('cf_hosted.mail_aliases'),
      $container->get('logger.channel.cf_hosted'),
    );
  }

  /**
   * {@InheritDoc}
   *
   * @return array
   *   All details of the message.
   *
   * To get through spam filters while allowing the reply-to feature to work, the
   * cforge mail server keeps an alias for every email in every cforge site.
   * Headers on outgoing mails must show the mail is sent from communityforge.net or a subdomain of it.
   * The mail server will then route replies to the correct address.
   */
  protected function prepareMessage(array $message) {
    $message = parent::prepareMessage($message);
    $headers = &$message['headers'];
    // Bug in MimeMailFormatHelper::mimemailHeaders() which puts <> around the mail and so it fails validation in phpMailer which uses var_filter().
    // fix in https://www.drupal.org/files/issues/2023-03-16/error-sending-email-16.patch
    $site_mail_alias = $this->mailAliases->getSiteAlias();
    // Cforge set up ensures all mails are sent from a verified domain, so we can
    // set all headers to the alias and ensure the reply-to button will work.
    // Careful coz the case-sensitive of email headers is ambiguous.
    if ($replyto = $headers['Reply-to'] ?? $message['reply-to']) {// Message is from a user
      // Drupal has already manipulated the headers in Drupal\Core\Mail\MailManager::doMail()
      // From: is set to the site.mail and the
      // Reply-To: is set to the actual sender account's email address
      // This was rather utopian because mail clients don't follow standards.

      $pattern = '/(?:<([^\s<>]+)>)?\s*"?([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})"?/';
      preg_match($pattern, $replyto, $matches);
      $real_mail = $matches[2];
      $uid = $this->database->select('users_field_data', 'u')
        ->fields('u', ['uid'])
        ->condition('mail', $real_mail)
        ->execute()->fetchField();
    }
    if (!empty($uid)) {
      $mail = $this->mailAliases->constructAlias($uid);
      $name = ucfirst(\Drupal\user\Entity\User::load($uid)->getDisplayName());
    }
    else {
      $name = $this->configFactory->get('system.site')->get('name');
      $mail = $site_mail_alias;
    }
    $headers['Sender'] = $mail;
    $headers['Return-Path'] = $site_mail_alias;
    $headers['Reply-to'] = $headers['From'] = "$name <$mail>";

    // In \Drupal\smtp\SMTPMailSystem::mail() from_name takes these precedents:
    // $message['params']['from_name']
    // $this->smtpConfig->get('smtp_fromname')
    // $this->configFactory->get('system.site')->get('name')
    $message['params']['from_name'] = $name;
    // $this->smtpConfig->get('smtp_from') So it must be left blank.
    // $message['params']['from_mail']
    // empty($message['from']
    $message['params']['from_mail'] = $mail;

    return $message;
  }

  protected function doMail(string $to, string $subject, string $message, array|string $additional_headers = [], string $additional_params = ''): bool {
    $success = mail(
      $to,
      $subject,
      $message,
      $additional_headers,
      $additional_params
    );
    if (!$success) {
      \Drupal::messenger()->addWarning(error_get_last());
    }
    return $success;
  }

}
