<?php

namespace Drupal\cf_hosted\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint to ensure that users do not enter emails on domains hosted by Cforge.
 *
 * @Constraint(
 *   id = "NonHostedEmail",
 *   label = @Translation("Checks that a user's email isn't on a cforge domain", context = "Validation"),
 * )
 */
class NonHostedEmail extends Constraint {

  public $message = 'There are no mailboxes hosted at @ @domain. You must use an external address.';

}