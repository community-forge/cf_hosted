<?php

namespace Drupal\cf_hosted\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\cf_hosted\MailAliases;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Validator for NonHostedEmail constraint.
 */
class NonHostedEmailValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * @var MailAliases
   */
  protected $mailAliases;

  function __construct(MailAliases $mail_aliases) {
    $this->mailAliases = $mail_aliases;
  }

  static function create($container) {
    return new static(
      $container->get('cf_hosted.mail_aliases')
    );
  }

  public function validate(mixed $items, Constraint $constraint) {
    $value = $items[0]->value;
    if (\Drupal::service('cf_hosted.mail_aliases')->isCforgeMail($value)) {
      $this->context->addViolation($constraint->message, ['@domain' => 'communityforge.net']);
    }
  }
}