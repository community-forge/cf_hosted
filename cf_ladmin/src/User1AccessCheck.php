<?php

namespace Drupal\cf_ladmin;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Access check for database update routes.
 */
class User1AccessCheck implements AccessInterface {

  /**
   * Checks whether the current user is user 1.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return AccessResult
   */
  public function access(AccountInterface $account) {
    if ($account->id() == 1) {
      return AccessResult::allowed()->cachePerUser();
    }
    else {
      return AccessResult::forbidden()->cachePerUser();
    }
  }

}
