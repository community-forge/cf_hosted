<?php

namespace Drupal\cf_ladmin;

use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Because the transaction collection is also the field ui base route, and
 * because views provides a superior listing to the entity's official
 * list_builder, this alters that view's route to comply with the entity.
 */
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_row_save' => [['migratePreRowSave']],
    ];
  }

  function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $migration = $event->getMigration();
    $row = $event->getRow();
    if ($migration->id() == 'd7_block') {
      if ($row->getDestinationProperty('id') == 'system_menu_block:setup') {
        throw new MigrateSkipRowException();
      }
    }
  }

}
