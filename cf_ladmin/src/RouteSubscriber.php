<?php

namespace Drupal\cf_ladmin;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * make certain routes only accessible to user 1 (and not to local admin)
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Make the following routes accessible to user 1 only.
    // though local admin has permission to administer permissions
    
    //@todo better
    return;
    $routes = ['user.admin_permissions', 'entity.taxonomy_vocabulary.edit_form', 'system.themes_page'];
    foreach ($routes as $route_id) {
      $collection->get($route_id)->setRequirements(['_is_user_1' => 'TRUE']);
    }

    // Local admin can only edit the current theme, not others
    $collection->get('system.theme_settings_theme')
      ->setRequirements(['_is_active_theme' => 'TRUE', '_role' => 'local_admin']);

  }

}
