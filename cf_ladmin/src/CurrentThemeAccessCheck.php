<?php

namespace Drupal\cf_ladmin;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Access check for database update routes.
 */
class CurrentThemeAccessCheck implements AccessInterface {

  protected $themeHandler;
  protected $routeMatch;

  public function __construct($theme_handler, $current_route_match) {
    $this->themeHandler = $theme_handler;
    $this->routeMatch = $current_route_match;
  }

  /**
   * Checks whether the current theme is the theme behind the link
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return AccessResult
   */
  public function access(AccountInterface $account, RouteMatch $route_match) {
    if(($this->themeHandler->getDefault() == $route_match->getParameter('theme')) || $account->id() == 1) {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::forbidden();
    }

    return $result->cachePerUser();
  }

}
