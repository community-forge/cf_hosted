<?php

namespace Drupal\cf_demo\DemoData;

/**
 * French demo data.
 */
class UserFr extends User {

  public static $firstNames = ['Alice', 'Bernard', 'Catherine', 'Dominique',
    'Emmanuel', 'Fanny', 'Grégoire', 'Henry', 'Isabelle', 'Jean', 'Kim',
    'Louis', 'Marianne', 'Nancy', 'Olivier', 'Pierre', 'Quentin', 'Renaud',
    'Sylvain', 'Thierry', 'Ursula', 'Véronique', 'William', 'Xavier', 'Yolande',
    'Zoe',
  ];
  public static $lastNames = ['Ansiaux', 'Boulanger', 'Colin', 'Dufour',
    'Enchemin', 'Ferrand', 'Gaillard', 'Humbert', 'Istace', 'Joubert', 'Keeler',
    'Lacroix', 'Marchand', 'Normand', 'Oliver', 'Parmentier', 'Quinet',
    'Rousseau', 'Sauvage', 'Tailleur', 'Ursullan', 'Valenduc', 'Wattiaux',
    'Xhonneux', 'Yernaux', 'Zapatero',
  ];

  public static $streets = [
    'Avenue du Prieuré',
    'Boulevard du Prince',
    'Allée des Noyers',
    'Grand Rue',
    'Chemin des Tanneurs',
    'Parc des Etangs',
    'Rue du Moulin',
    'Route de Belleroche',
    'Impasse des Pinsons',
    'Rue des Panoramas',
  ];

  public static $hoods = [
    'La Roche',
    'Baronville',
    'Moustiers',
    'Templeuve',
    'Aisemont',
  ];


  // Get a random location in the Geneva area.
  public static function randCoordinate() {
    return parent::randLocation(46.2181503, 6.0538799, 46.1756124, 6.1216862);
  }

}
