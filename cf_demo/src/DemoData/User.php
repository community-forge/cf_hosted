<?php

namespace Drupal\cf_demo\DemoData;

/**
 * Base class to create demo data in any lanaguage.
 */
abstract class User {

  public static $firstNames = [];
  public static $lastNames = [];
  public static $names = [];
  public static $streets = [];
  #public static $hoods = [];

  /**
   * Get a random neighbourhood.
   */
  public static function randNeighbourhood() {
    $key = array_rand(static::$hoods);
    return static::$hoods[$key];
  }

  /**
   * Get a random street.
   */
  public static function randStreet() {
    $key = array_rand(static::$streets);
    return static::$streets[$key];
  }

  /**
   * Get a random phone number.
   */
  public static function randPhone() {
    return '0' . rand(1000, 9999) . ' ' . rand(100000, 999999);
  }

  /**
   * Return a randomised firstname and last name.
   */
  public static function getName() {
    static $firstnames, $lastnames = [];
    if (!$firstnames) {
      $firstnames = static::$firstNames;
      shuffle($firstnames);
    }
    if (!$lastnames) {
      $lastnames = static::$lastNames;
      shuffle($lastnames);
    }
    return [array_pop($firstnames), array_pop($lastnames)];
  }

  /**
   * Return the number of demo user names.
   */
  public static function countUsers() {
    return count(static::$firstNames);
  }

  // Get a random location away from the poles.
  public static function randLocation($north = 84, $east = 180, $south = -84, $west= -180) {
    $lat = static::between($east, $west);
    $lon = static::between($north, $south);
    return "POINT($lat, $lon)";
  }

  public static function between($a, $b) : float {
    // Deprecated function: Implicit conversion from float 60538.799 to int loses precision
    // maybe prob with negative numbers?
    $rand = rand(intval($a*10000), intval($b*10000));
    return $rand/10000;
  }

}
