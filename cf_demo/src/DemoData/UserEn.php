<?php

namespace Drupal\cf_demo\DemoData;

/**
 * Demo data in English.
 */
class UserEn extends User {

  public static $firstNames = ['Alice', 'Barry', 'Cherry', 'Darius',
    'Ebeneezer', 'Fanny', 'Garry', 'Harry', 'Irma', 'Jerry', 'Kerry', 'Larry',
    'Murray', 'Nancy', 'Oliver', 'Perry', 'Quentin', 'Ruby', 'Sylvester',
    'Terry', 'Ursula', 'Veronica', 'William', 'Xanadu', 'Yuri', 'Zoe',
  ];
  public static $lastNames = ['Archer', 'Baker', 'Cooper', 'Draper', 'Earl',
    'Fisher', 'Gardener', 'Hooper', 'Ingram', 'Joyner', 'Keeler', 'Laker',
    'Miller', 'Nutter', 'Oliver', 'Parker', 'Quaker', 'Rider', 'Saddler',
    'Turner', 'Usher', 'Vicker', 'Wainwright', 'X', 'Yardley', 'Zapatero',
  ];

  public static $streets = [
    'Aberdeen Avenue',
    'Birmingham Boulevard',
    'Dunstable Drive',
    'Harwich Highway',
    'London lane',
    'Paisley Park',
    'Rochester Road',
    'Sheffield Street',
    'Tavistock Track',
    'Warminster Way',
  ];

  public static $hoods = [
    'Apple Green',
    'Black Hills',
    'Cow Common',
    'Ditchwater',
    'Elephantine',
  ];


  // Get a random location in the Geneva area.
  public static function randCoordinate() {
    return parent::randLocation(46.2181503, 6.0538799, 46.1756124, 6.1216862);
  }


}
