<?php

namespace Drupal\cf_demo;

use Symfony\Component\DependencyInjection\Reference;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the language manager service.
 */
class CfDemoServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('string_translation')
      ->setClass('Drupal\cf_demo\DemoTranslationManager')
      ->addArgument(new Reference('config.factory'))
      ->addArgument(new Reference('messenger'));
  }

}
