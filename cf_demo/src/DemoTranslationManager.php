<?php

namespace Drupal\cf_demo;

use Drupal\Core\Language\LanguageDefault;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use \Drupal\Core\StringTranslation\TranslationManager;

/**
 * Defines puts cforge modules' untranslated strings in the message box when a site is in demo mode
 */
class DemoTranslationManager extends TranslationManager {

  /**
   * TRUE if the site is in Demo mode.
   * @var bool
   */
  private $demo;

  /**
   * The Drupal messenger service
   * @var MessengerInterface
   */
  private $messenger;

  /**
   * Regex pattern
   * @var string
   */
  private $pattern;

  /**
   * Constructs a TranslationManager object.
   *
   * @param \Drupal\Core\Language\LanguageDefault $default_language
   *   The default language.
   */
  public function __construct(LanguageDefault $default_language, ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    parent::__construct($default_language);
    $this->messenger = $messenger;
    $this->demo = $config_factory->get('cforge.settings')->get('demo_mode');

    $modules = ['cforge\/', 'community_tasks', 'cf_ladmin', 'cf_hosted', 'mutual_credit', 'smallads', 'masquerade_nominate'];
    $this->pattern = '/'.implode('|', $modules).'/';
  }

  /**
   * {@inheritdoc}
   */
  public function getStringTranslation($langcode, $string, $context) {
    $translation = parent::getStringTranslation($langcode, $string, $context);

    if (!$translation and $this->demo and $this->defaultLangcode != 'en') {
      if ($module = $this->isCfModule()) {
        // Return the untranslated string in square brackets
        $this->messenger->addWarning("\"$string\" untranslated in module $module", FALSE);
        return '['.$string.']';
      }
    }
    return $translation ?? FALSE;
  }

  private function isCfModule() {
    $results = $functions = [];
    $trace = debug_backtrace();
    foreach (array_slice($trace, 1) as $block) {
      if (isset($block['file'])) {
        if (isset($block['class']) and $block['class'] == 'Drupal\Core\Utility\Token') {
          continue;
        }
        if (preg_match($this->pattern, $block['file'], $matches)) {
          $results[] = $matches[0];
          break;
        }
      }
    }
    return implode(' ', $results);
  }

}
