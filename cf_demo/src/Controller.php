<?php

namespace Drupal\cf_demo;
use \Drupal\user\Entity\User;
use \Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Demo module controller.
 */
class Controller {

  /**
   * Login to any account with just its account id.
   */
  function login(int $uid) {
    if ($uid <> 1 and $user = User::load($uid)) {
      \Drupal::service('masquerade')->switchTo($user);
      \Drupal::service('session')->remove('masquerading');
      return new RedirectResponse('/news');
    }
    return new RedirectResponse('<front>');
  }
}

