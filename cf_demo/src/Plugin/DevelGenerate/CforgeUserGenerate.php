<?php

namespace Drupal\cf_demo\Plugin\DevelGenerate;

use Drupal\devel_generate\Plugin\DevelGenerate\UserDevelGenerate;
use Drupal\user\Entity\User;
use Drupal\mcapi\Entity\Wallet;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a UserDevelGenerate plugin.
 *
 * @DevelGenerate(
 *   id = "cforge_user",
 *   label = @Translation("Generate Hamlets users"),
 *   description = @Translation("Generate 26 users with real names."),
 *   url = "user/cforge",
 *   permission = "administer devel_generate",
 *   settings = {
 *     "kill" = FALSE,
 *     "pass" = ""
 *   }
 * )
 */
class CforgeUserGenerate extends UserDevelGenerate {

  private $configFactory;
  private $accountSwitcher;
  private $demoDataClass;

  /**
   * Constructor.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $messenger, $language_manager, $module_handler, $string_translation, $user_storage, $date_formatter, $time, $role_storage, $config_factory, $account_switcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $messenger, $language_manager, $module_handler, $string_translation, $user_storage, $date_formatter, $time, $role_storage);
    $this->configFactory = $config_factory;
    $this->accountSwitcher = $account_switcher;
    // @todo tidy this up.
    \Drupal::moduleHandler()->loadInclude('cf_demo', 'install');
    $class = cf_demo_demodata_class();
    $this->demoDataClass = new $class();
  }

  /**
   * 
   * @param ContainerInterface $container
   * @param array $configuration
   * @param type $plugin_id
   * @param type $plugin_definition
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $entity_type_manager,
      $container->get('messenger'),
      $container->get('language_manager'),
      $container->get('module_handler'),
      $container->get('string_translation'),
      $entity_type_manager->getStorage('user'),
      $container->get('date.formatter'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager')->getStorage('user_role'),
      $container->get('config.factory'),
      $container->get('account_switcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) : array {
    $form = parent::settingsForm($form, $form_state);
    $form['num']['#value'] = 26;
    $form['num']['#disabled'] = 26;
    unset($form['roles']);
    unset($form['pass']);
    // 1 year ago.
    $form['time_range']['#default_value'] = 31536000;
    $form['time_range']['#type'] = 'hidden';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function generateElements(array $values) : void {
    $countries = FieldConfig::load('user.user.address')->getSetting('available_countries');

    if ($values['kill']) {
      $uids = $this->userStorage->getQuery()->accessCheck(FALSE)
        ->condition('uid', 1, '>')
        ->execute();
      $users = $this->userStorage->loadMultiple($uids);
      $this->userStorage->delete($users);
    }
    // We only have demo data in two languages
    // before saving accounts we need to switch to user 1 otherwise the wallet
    // name will be wrongly derived.
    $this->accountSwitcher->switchTo(User::load(1));
    $count = $this->demoDataClass->countUsers();
    for ($i = 0; $i < $count; $i++) {
      $account = $this->userStorage->create([
        'name' => 'a',
        'pass' => 'demo',
        'mail' => 'temp@example.com',
        'status' => 1,
        'roles' => [AccountInterface::AUTHENTICATED_ROLE],
      ]);

      $this->populateFields($account);
      if ($i < 4) {
        $account->addRole('committee');  // RID_COMMITTEE
      }
      if (!rand(0, 8)){
        $account->masquerade_nominees = [];
      }
      // 1 user per year.
      $account->address->country_code = reset($countries);
      $this->populateUser($account);
      $account->created->value = \Drupal::time()->getRequestTime() - ($i + 1) * 30000000;
      $account->newWallet = Wallet::create(['holder' => $account]);
      $account->save();
    }
    $this->accountSwitcher->switchBack();
    $this->setMessage(t('@count users created.', ['@count' => $count]));
  }

  /**
   * {@inheritdoc}
   */
  public function populateUser($account) {
    // Populate all fields with sample values.
    $account->contactme->mob = $this->demoDataClass->randPhone();
    $account->contactme->tel = $this->demoDataClass->randStreet();
    $account->address->address_line1 = $this->demoDataClass->randStreet();
    $account->address->dependent_locality = $this->demoDataClass->randNeighbourhood();
    $loc = $this->demoDataClass->randCoordinate();
    if (property_exists($account, 'coordinates')) {
      $account->coordinates->value = $loc;
    }
    list($firstname, $lastname)  = $this->demoDataClass->getName();
    $mail = $firstname . '@example.com';
    $account->setEmail($mail);
    $account->init->value = $mail;
    $account->name->value = $firstname . ' ' . $lastname;
    $account->address->given_name = $firstname;
    $account->address->family_name = $lastname;
  }

}
