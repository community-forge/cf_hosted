<?php

namespace Drupal\cf_demo\Plugin\Mail;

use Drupal\Core\Mail\Plugin\Mail\PhpMail;
use Drupal\Core\Mail\MailFormatHelper;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a mail backend that prints mails to screen in demo_mode
 *
 * @Mail(
 *   id = "cf_demo_mailer",
 *   label = @Translation("Hamlets Demo Mailer"),
 *   description = @Translation("Prints example.com emails to screen")
 * )
 */
class CforgeDemoMailer extends PhpMail implements ContainerFactoryPluginInterface {

  protected $messenger;

  /**
   * @param ConfigFactoryInterface $config_factory
   * @param MessengerInterface $messenger
   */
  public function __construct(MessengerInterface $messenger) {
    parent::__construct();
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function format(array $message) {
    $eol = PHP_EOL;
    $boundary = uniqid('np');
    $message['headers']['Sender'] = '"'
      . str_replace('"', '', $this->configFactory->get('system.site')->get('mail'))
      . '" <' . $default_from . '>';


    // Join the body array into one string.
    $html = implode("\n\n", $message['body']);

    $message['headers']['Content-Type'] = 'multipart/alternative;boundary="' . $boundary . '"';
    //$message['headers']['MIME-Version'] = '1.0';

    $raw_message = 'This is a MIME encoded message.';
    $raw_message .= $eol . $eol . "--" . $boundary . $eol;
    $raw_message .= "Content-Type: text/plain;charset=utf-8" . $eol . $eol;
    $raw_message .= MailFormatHelper::htmlToText($html);
    $raw_message .= $eol . $eol . "--" . $boundary . $eol;
    $raw_message .= "Content-Type: text/html;charset=utf-8" . $eol . $eol;
    $raw_message .= $html;
    $raw_message .= $eol . $eol . "--" . $boundary . "--";
    $message['body'] = $raw_message;

    return $message;
  }

  /**
   * {@inheritDoc}
   *
   * Don't send mail to example users.
   */
  public function mail(array $message) {
    if (stripos($message['to'], 'example.com')) {
      $output[] = "to: ".$message['to'];
      $output[] = "from: ".$message['from'];
      foreach ($message['headers'] as $key => $value) {
        $output[] = "$key: $value";
      }
      $output[] = '';
      $output[] = $message['body'];
      // This works for plaintext mails.
      $markup = Markup::create('<pre>'.implode('<br>', $output).'</pre>');
      $this->messenger->addStatus($markup);
      return TRUE;
    }
    return parent::mail($message);
  }

}
