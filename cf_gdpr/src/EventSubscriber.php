<?php

namespace Drupal\cf_gdpr;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Check the user has consented and redirect them to the consent form.
 */
class EventSubscriber implements EventSubscriberInterface {

  const IGNORE_ROUTES = [
    'cf_gdpr.form',
    'user.reset',
    'user.reset.login',
    'user.logout',
    'entity.user.edit_form',
    'system.403',
    'system.404'
  ];

  /**
   * @param GetResponseEvent $event
   */
  public function checkConsent(RequestEvent $event) {
    $account = \Drupal::currentUser();
    if (!in_array(\Drupal::service('current_route_match')->getRouteName(), static::IGNORE_ROUTES)) {
      /** @var RouteMatchInterface $route_match */
      if ($account->isAuthenticated()) {
        if (!cf_gdpr_signed($account->id())) {
          $event->setResponse(new RedirectResponse('/gdpr/accept'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    $events[KernelEvents::REQUEST][] = ['checkConsent'];
    return $events;
  }

}


