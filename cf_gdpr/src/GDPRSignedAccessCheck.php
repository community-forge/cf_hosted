<?php

namespace Drupal\cf_gdpr;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on login status of current user.
 */
class GDPRSignedAccessCheck implements AccessInterface {

  /**
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route) {
    $required_status = filter_var($route->getRequirement('_gdpr_signed'), FILTER_VALIDATE_BOOLEAN);
    $actual_status  = cf_gdpr_signed($account->id());
    $access_result = AccessResult::allowedif($actual_status == $required_status);
    return $access_result->cachePerUser();
  }

}
