<?php

namespace Drupal\cf_gdpr;

use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Controller\ControllerBase;


class PageController extends ControllerBase {


  /**
   * Page if the user declines to accept GDPR.
   *
   * @return type
   */
  function declined() : array {
    return [
      '#markup' => $this->t("Either log in again and click 'agree' or wait for a member of the committee to contact you.")
    ];
  }

  /**
   * Page showing GDPR statement.
   *
   * @return array
   */
  function statement() : array {
    $gdpr_settings = $this->config('cf_gdpr.settings');
    $committee = [];
    foreach (gdpr_get_committee() as $account) {
      $committee[$account->getDisplayName()] = t('Committee member');
    }
    return [
      '#theme' => 'gdpr_statement',
      '#responsible' => [
        $this->config('system.site')->get('name') => $committee
      ],
      '#httphost' => $_SERVER['HTTP_HOST'],
      '#site_mail' => \Drupal::config('system.site')->get('mail'),
      '#host_org' => Link::fromTextAndUrl(
        $this->config('system.site')->get('name'),
        Url::fromUri($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'])
      )
    ];
  }


  function statementTitle() : \Drupal\Component\Render\MarkupInterface {
    return $this->t(
      'GDPR statement for %site_name',
      ['%site_name' => $this->config('system.site')->get('name')]
    );
  }
}
