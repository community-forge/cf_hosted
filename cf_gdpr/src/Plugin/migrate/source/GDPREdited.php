<?php

namespace Drupal\cf_gdpr\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\Variable;

/**
 * Drupal 7 Privacy Policy edited.
 *
 * @MigrateSource(
 *   id = "d7_gdpr_edited",
 *   source_module = "cf_gdpr"
 * )
 */
class GDPREdited extends Variable {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $nid = $this->variableGet('cookiecontrol_privacynode', 1);
    $query = $this->select('node_revision', 'nr')
      ->fields('nr', ['uid', 'timestamp'])
      ->condition('nid', $nid)
      ->orderBy('vid', 'DESC');
    return $query;
  }

  protected function values() {
    $values['id'] = 'GDPREdited';
    $values += $this->prepareQuery()->execute()->fetchAssoc();
    if (!$values['uid']) {
      $values['uid'] = 1;
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'uid' => $this->t('Editor id'),
      'timestamp' => $this->t('Last edited time')
    ];
  }

}
