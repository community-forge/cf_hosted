<?php

namespace Drupal\cf_gdpr;

use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use \Drupal\Core\Render\Markup;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for GDPR consent
 */
class ConsentForm extends FormBase implements ContainerInjectionInterface {

  protected $userData;

  function __construct($user_data_store) {
    $this->userData = $user_data_store;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cf_gdpr_consent';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return [
      '#title' => $this->t('Do you accept the GDPR terms and conditions which you should have received by mail? Choose ONE of the following:'),
      'conditions' => [
        '#markup' => Markup::Create('<p>'.implode("</p>\n<p>", cf_gdpr_load_responsibilities()).'</p>')
      ],
      'agree' => [
        '#type' => 'item',
        '#markup' => $this->t('I agree that my data may be shared with other members. I promise to use the data accessible on this site only within the context of the activities of this platform.'),
        '#weight' => 1
      ],
      'yes' => [
        '#type' => 'submit',
        '#value' => $this->t('I agree'),
        '#weight' => 2
      ],
      'disagree' => [
        '#type' => 'item',
        '#markup' => $this->t("I don't quite understand. I am not sure. I refuse. I wish to be contacted by an administrator."),
        '#weight' => 3
      ],
      'no' => [
        '#type' => 'submit',
        '#value' => $this->t("I do not agree"),
        '#weight' => 4
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
     $account = User::load(\Drupal::currentUser()->id());
     $accepted = $form_state->getTriggeringElement()['#parents'][0] == 'yes';
     $this->userData->set('cf_gdpr', $account->id(), 'consent', $accepted);
     if ($accepted) {
       if (Role::load('trader') and !$account->hasRole('trader')) {
         $account->addRole('trader');
         $account->save();
       }
       $this->messenger()->addMessage($this->t('Thanks for agreeing to the GDPR conditions. You can now continue to use the site normally.'));

       $url = Url::fromUserInput('/gdpr');
       $form_state->setRedirectUrl($url);
     }
     else {
       if ($account->hasRole('trader')) {
         $account->removeRole('trader');
         $account->save();
       }
       // Mail all the members of the committee about this event.
       foreach (gdpr_get_committee() as $committee) {
         \Drupal::service('plugin.manager.mail')->mail(
           'cf_gdpr',
           'gdpr_no',
           $committee->getEmail(),
           $committee->getPreferredLangcode(),
           ['user' => $account, 'recipient' => $committee]
         );
       }
       $form_state->setRedirect('cf_gdpr.declined');
       user_logout();
     }
  }

}
